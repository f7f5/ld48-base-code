IMPACTJS_PATH=/home/nuno/Dev/Jogos/ImpactJS/impact-1.24/impact/

TARGET_PATH=/home/nuno/Dev/Jogos/ld48/new3
BASENAME=$(lastword $(subst /, ,${TARGET_PATH}))
WWW_LINK=/var/www/html/${BASENAME}

impact:
	mkdir ${TARGET_PATH}
	cp -r analytics css ecrans ${TARGET_PATH}
	cp -r ${IMPACTJS_PATH}/* ${TARGET_PATH}
	chmod a+w ${TARGET_PATH}/lib/game/levels
	cp index.html ${TARGET_PATH}
	cp -r f7f5 ${TARGET_PATH}/lib/
	cp -r game ${TARGET_PATH}/lib/
	cp media/* ${TARGET_PATH}/media/
	su -c "ln -s ${TARGET_PATH} /var/www/html/"


clean:
	su -c "rm ${WWW_LINK}"
	rm -rf "${TARGET_PATH}"
