#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import json
from datetime import datetime
import webapp2
from google.appengine.ext import ndb


class Log(ndb.Model):
    """Logged event."""
    session_id = ndb.StringProperty()
    remote_ip = ndb.StringProperty()
    event = ndb.StringProperty()
    timestamp = ndb.DateTimeProperty()


class MainHandler(webapp2.RequestHandler):
    def get(self):
        self.response.write('Hello world!')
        
    def post(self):
        #self.response.headers.add_header("Access-Control-Allow-Origin", "*")
        session_id = self.request.get('session_id')
        event = self.request.get('event')
        timestamp = datetime.strptime(self.request.get('timestamp'), "%Y%m%d%H%M%S")
        remote_ip = self.request.remote_addr
        g = Log(session_id=session_id, event=event, timestamp=timestamp, remote_ip=remote_ip)
        g.put()
        self.response.write('OK')


class CsvHandler(webapp2.RequestHandler):
    def get(self):
        import cStringIO
        import csv
        output = cStringIO.StringIO()
        spamwriter = csv.writer(output)
        for log in Log.query():
            spamwriter.writerow([log.session_id, log.remote_ip, log.event, log.timestamp])
        output.seek(0)    
        self.response.headers['Content-Type'] ='text/csv'
        self.response.headers['Content-Disposition'] = 'attachment; filename="stats.csv"'
        self.response.out.write(output.read())

            
# Highscores
class Score(ndb.Model):
    """Score."""
    remote_ip = ndb.StringProperty()
    name = ndb.StringProperty()
    score = ndb.IntegerProperty()
    timestamp = ndb.DateTimeProperty()



def serialize_datetime(value):
    if isinstance(value, datetime):
        return value.isoformat()
    raise TypeError("Unknown type")


class HighscoreHandler(webapp2.RequestHandler):
    MAX_NAME_LENGTH = 20

    def return_table(self, new_entry=None):
        top = Score.query().order(-Score.score, Score.timestamp).fetch(limit=10)
        if new_entry:
            for i in range(len(top)):
                if top[i].score < new_entry.score:
                    top[i:i] = [new_entry]
                    break
            top = top[:10]
        text = json.dumps(map(lambda x: x.to_dict(exclude=('remote_ip', )), top), default=serialize_datetime)
        self.response.headers['Content-Type'] = 'application/json'
        self.response.out.write(text)
        
    def get(self):
        self.return_table()

    def post(self):
        name = self.request.get('name')[:self.MAX_NAME_LENGTH]
        score = int(self.request.get('score'))
        timestamp = datetime.now()
        remote_ip = self.request.remote_addr
        s = Score(name=name, score=score, timestamp=timestamp, remote_ip=remote_ip)
        s.put()
        # nbd has weak consistency, we need to insert the result manually to guarantee
        # it's seen.
        self.return_table(s)


app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/csv/', CsvHandler),
    ('/scores/', HighscoreHandler),
], debug=True)
