window.d5_anlytics = (function() {

  // http://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
  var generateUUID = function() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
    };

   var url = '/'  //'http://localhost:8080/'; // FIXME
   var session_id = generateUUID();
   
   // http://stackoverflow.com/questions/2998784/how-to-output-integers-with-leading-zeros-in-javascript
   function pad(num, size){ return ('000000000' + num).substr(-size); }
   
   var formatDate = function() {
       var dt = new Date();
       return dt.getUTCFullYear().toString() + pad(dt.getUTCMonth()+1, 2) + pad(dt.getUTCDate(), 2) +
              pad(dt.getUTCHours(), 2) + pad(dt.getUTCMinutes(), 2) + pad(dt.getUTCSeconds(),2);
   }

   this.log = function(event) {
       var dtStr = formatDate();
       var msg = {
           session_id: session_id,
           event: event,
           timestamp: dtStr
       }
       
    
       
    $.ajax({ 
        type: 'POST', 
        url: url, 
        data: msg, 
        success: function(data, textStatus, XMLHttpRequest) { 
        }, 
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
        } 
        });
   };
   
   this.score = function(name, score, callback) {
       var msg = {
           //session_id: session_id,
           name: name,
           score: score
       }
       
    
        $.ajax({ 
            type: 'POST', 
            url: url + 'scores/', 
            data: msg, 
            success: function(data, textStatus, XMLHttpRequest) {
                console.log("Score posted");
                callback(data);
            }, 
            error: function(XMLHttpRequest, textStatus, errorThrown) { 
            } 
        });
   };
   
   this.highscore_table = function(callback) {
        console.log("Retrieving High Score table");
        $.ajax({ 
            type: 'GET', 
            url: url + 'scores/', 
            success: function(data, textStatus, XMLHttpRequest) { 
                callback(data);
            }, 
            error: function(XMLHttpRequest, textStatus, errorThrown) { 
            } 
        });
   };
   return this;
});
