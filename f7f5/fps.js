ig.module(
	'f7f5.fps'
)
.requires(
	'dom.ready',
	'impact.system'
)
.defines(function(){ "use strict";


ig.System.inject({	
	run: function() {
		f7f5fpslogger.beforeRun();
		this.parent();
	},
	
});


var FPS = ig.Class.extend({
	fpsTimer: new ig.Timer(),
        debugTickAvg: 0.016,
	
	init: function() {
            this.debugRealTime = Date.now();
            this.fpsTimer.set(10);
	},
	
	
	beforeRun: function() {
            var timeBeforeRun = Date.now();
            this.debugTickAvg = this.debugTickAvg * 0.8 + (timeBeforeRun - this.debugRealTime) * 0.2;
            this.debugRealTime = timeBeforeRun;
            if (this.fpsTimer.delta() >= 0) {
                var fps = Math.round(1000/this.debugTickAvg);
                console.log("FPS:", fps);
                this.fpsTimer.reset();
            }
	}
});

var f7f5fpslogger = new FPS();

});


