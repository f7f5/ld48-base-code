ig.module( 
	'f7f5.highscores' 
)
.requires(
    'impact.game',
)
.defines(function(){

    
HighScoresScreen = ig.Game.extend({
    nameInput: false,
    name: "",
    characters: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ' '],

    init: function() {
        var self=this;
        if (ig.global['FinalScore']  && (ig.global['HighScores'].length < 10 || (
            ig.global['FinalScore'] > ig.global['HighScores'][ig.global['HighScores'].length-1].score)))
        {
            this.nameInput = true;
            ig.input.bind(ig.KEY._0, '0');
            ig.input.bind(ig.KEY._1, '1');
            ig.input.bind(ig.KEY._2, '2');
            ig.input.bind(ig.KEY._3, '3');
            ig.input.bind(ig.KEY._4, '4');
            ig.input.bind(ig.KEY._5, '5');
            ig.input.bind(ig.KEY._6, '6');
            ig.input.bind(ig.KEY._7, '7');
            ig.input.bind(ig.KEY._8, '8');
            ig.input.bind(ig.KEY._9, '9');
            ig.input.bind(ig.KEY.A, 'A');
            ig.input.bind(ig.KEY.B, 'B');
            ig.input.bind(ig.KEY.C, 'C');
            ig.input.bind(ig.KEY.D, 'D');
            ig.input.bind(ig.KEY.E, 'E');
            ig.input.bind(ig.KEY.F, 'F');
            ig.input.bind(ig.KEY.G, 'G');
            ig.input.bind(ig.KEY.H, 'H');
            ig.input.bind(ig.KEY.I, 'I');
            ig.input.bind(ig.KEY.J, 'J');
            ig.input.bind(ig.KEY.K, 'K');
            ig.input.bind(ig.KEY.L, 'L');
            ig.input.bind(ig.KEY.M, 'M');
            ig.input.bind(ig.KEY.N, 'N');
            ig.input.bind(ig.KEY.O, 'O');
            ig.input.bind(ig.KEY.P, 'P');
            ig.input.bind(ig.KEY.Q, 'Q');
            ig.input.bind(ig.KEY.R, 'R');
            ig.input.bind(ig.KEY.S, 'S');
            ig.input.bind(ig.KEY.T, 'T');
            ig.input.bind(ig.KEY.U, 'U');
            ig.input.bind(ig.KEY.V, 'V');
            ig.input.bind(ig.KEY.W, 'W');
            ig.input.bind(ig.KEY.X, 'X');
            ig.input.bind(ig.KEY.Y, 'Y');
            ig.input.bind(ig.KEY.Y, 'Z');
            ig.input.bind(ig.KEY.Y, 'Z');
            ig.input.bind(ig.KEY.Y, 'Z');
            ig.input.bind(ig.KEY.BACKSPACE, 'del');
            ig.input.bind(ig.KEY.DELETE, 'del');
            ig.input.bind(ig.KEY.SPACE, ' ');
            ig.input.bind(ig.KEY.ENTER, 'enter');
        } else {
            d5.highscore_table(
                function(data) {
                    console.log("ok");
                    ig.global['HighScores'] = data;
                }
            );
            ig.input.bind(ig.KEY.SPACE, 'fire');
        }
//         if (ig.global['FinalScore']) {
//             d5.score("Player 1", ig.global['FinalScore'],
//                 function(data) {
//                     console.log("ok");
//                     self.highscores = data;
//             });
//         } else {
//             d5.highscore_table(
//                 function(data) {
//                     console.log("ok");
//                     self.highscores = data;
//                 }
//             );
//         }
    },
    update: function() {
        if (this.nameInput) {
            for (var l=0; l< this.characters.length; l++) {
                let letter = this.characters[l];
                if (ig.input.pressed(letter)) {
                    if (this.name.length < 10) {
                        this.name += letter;
                    }
                }
            }
            if (ig.input.pressed("del")) {
                if (this.name.length > 0) {
                    this.name = this.name.substring(0, this.name.length-1);
                }
            }
            if (ig.input.pressed("enter")) {
                var self=this;
                this.nameInput = false;
                d5.score(this.name, ig.global['FinalScore'],
                    function(data) {
                        console.log("ok");
                        ig.global['HighScores'] = data;
                        ig.input.bind(ig.KEY.SPACE, 'fire');
                });
            }
        } else {
            if( ig.input.pressed('fire')) {
            ig.system.setGame( ig.global['TitleScreen'] ); 
            return;
            }
        }
    },
    draw: function() {
        this.parent();
        if (this.nameInput) {
            this.drawInputScreen();
            return;
        }
        highscores = ig.global['HighScores'];
        if (highscores !== null) {
            var ctx = ig.system.context;
    
            ctx.fillStyle  = '#ffff00';
            ctx.font = '40px Titan One';            
            ctx.textAlign = 'center';
            var middle = ig.system.realWidth/2;
            ctx.fillText("HIGHSCORES",  middle, 100);
            var left = middle-200;
            var right = middle+200;
            ctx.font = '20px Titan One';
            
            for (var i=0; i<highscores.length; i++) {
                let hs = highscores[i];
                let y = 150 + i*25;
                ctx.textAlign = 'left';
                ctx.fillStyle  = '#00ff00';
                ctx.fillText(hs.name,  left, y);
                ctx.fillStyle  = '#ff00ff';
                ctx.textAlign = 'right';
                ctx.fillText(hs.score,  right, y);
            }
        }
    },
    printableNameInput: function() {
        let l=this.name.length;
        value = "";
        for (var i=0; i<10; i++) {
            if (i < l) {
                value += this.name[i];
            } else {
                value += "-";
            }
        }
        return value;
    },
    drawInputScreen: function() {
        var ctx = ig.system.context;
        ctx.fillStyle  = '#ffff00';
        ctx.font = '40px Titan One';            
        ctx.textAlign = 'center';
        var middle = ig.system.realWidth/2;
        ctx.fillText("CONGRATULATIONS",  middle, 100);
        ctx.font = '30px Titan One';
        ctx.fillStyle  = '#ff00ff';
        ctx.fillText("Please enter your name",  middle, 200);
        ctx.fillText("into the Hall of Fame",  middle, 250);
        ctx.fillStyle  = '#00ff00';
        ctx.fillText(this.printableNameInput(),  middle, 350);
    }
})
});

