ARENA_WIDTH=630;
ARENA_HEIGHT=450;

ig.module( 
	'game.main' 
)
.requires(
	'impact.game',
	'impact.font',
        'impact.debug.debug',
        'f7f5.highscores',
        'game.levels.level1'
)
.defines(function(){

MyGame = ig.Game.extend({
	
	// Load a font
	font: new ig.Font( 'media/04b03.font.png' ),
	score: 0,
        over: false,
	
	init: function() {
            // Initialize your game here; bind keys etc.
//             ig.input.bind(ig.KEY.Q, 'left');
//             ig.input.bind(ig.KEY.P, 'right');
            ig.input.bind(ig.KEY.SPACE, 'fire');
            this.loadLevelDeferred(ig.global['LevelLevel1']);  
	},
	
	update: function() {
            // Update all entities and backgroundMaps
            this.parent();
            if( (document.getElementById('game-over').style.display == 'block') && ig.input.pressed('fire')) {
                document.getElementById('game-over').style.display = 'none';
//                 this.endMusic.stop();
                ig.system.setGame(HighScoresScreen);
                return;
            }    
            // Add your own, additional update code here
            if (this.over) return;
            this.addScore(1);
            if( ig.input.pressed('fire')) {
                this.death();
            }
	},
	
	draw: function() {
		// Draw all entities and backgroundMaps
		this.parent();
		
		
		// Add your own drawing code here
		var x = ig.system.width/2,
			y = ig.system.height/2;
		
		this.font.draw( 'It Works!', x, y, ig.Font.ALIGN.CENTER );
	},
        
        addScore: function(points) {
            this.score += points;
            document.getElementById('score').innerHTML = this.score.toString();
        },
        
        death: function() {
            document.getElementById('game-over').style.display = 'block';
            this.over = true;
//         this.gameMusic.stop();
           d5.log("end: " + this.score.toString());
           ig.global['FinalScore'] = this.score;
        }
});


TitleScreen = ig.Game.extend({
//     startMusic: new ig.Sound( 'media/11.ogg' ),
    init: function() {
        ig.input.bind(ig.KEY.SPACE, 'fire');
//         this.startMusic.volume = 1.0;
//         this.startMusic.play();
        
    },
    update: function() {
        // Check for buttons; start the game if pressed
        if( ig.input.pressed('fire')) {
//             this.startMusic.stop();
//             d5.log("start");
            ig.system.setGame( MyGame );
            return;
        }
    }
});


// Start the Game with 60fps, a resolution of 320x240, scaled
// up by a factor of 2
ig.main( '#canvas', TitleScreen, 60, ARENA_WIDTH, ARENA_HEIGHT, 1 );
d5.highscore_table(
    function(data) {
        window.HighScores = data;
    }
);
});
