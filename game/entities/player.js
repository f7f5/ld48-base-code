ig.module(
    'game.entities.player'
)
.requires(
    'impact.entity'
)
.defines(function(){

// A 2D top-down player
EntityPlayer = ig.Entity.extend({
    // TODO como alterar isto ne weltmeister?
    size: {x: 38, y: 50},
    maxVel: {x:200, y:200},
    image: new ig.Image('media/sabreman.png'),
    type: ig.Entity.TYPE.A,
    checkAgainst: ig.Entity.TYPE.B,
    collides: ig.Entity.COLLIDES.PASSIVE,
    
    update: function() {
        if( ig.input.state('left') ) {
            this.vel.x = -SPEED;
        }
        else if( ig.input.state('right') ) {
            this.vel.x = SPEED;
        } else {
            this.vel.x = 0;
        }
        if( ig.input.state('up') ) {
            this.vel.y = -SPEED;
        }
        else if( ig.input.state('down') ) {
            this.vel.y = SPEED;
        }
        else {
            this.vel.y = 0;
        }
        
        // normalize if diagonal
        if ((this.vel.x != 0) && (this.vel.y != 0)) {
            this.vel.x = this.vel.x * 0.707106781187; //Math.cos(0.25*Math.PI);
            this.vel.y = this.vel.y * 0.707106781187; //Math.sin(0.25*Math.PI);
        }
//         if ((this.vel.x != 0) || (this.vel.y != 0)) {
//             if (!this.s2) {
//                 this.sound2.loop = true;
//                 this.sound2.volume = 1;
//                 this.sound2.play();
//                 this.s2 = true;
//             }
//         } else {
//             if (this.s2) {
//                 this.sound2.stop();
//                 this.s2 = false;
//             }
//         }
        this.parent();
        
    },
        
    draw: function() {
        this.image.draw(this.pos.x, this.pos.y); //scale=???
    }
});



});
